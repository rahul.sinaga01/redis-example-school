package com.school.controller;

import com.school.entity.student.Student;
import com.school.service.student.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    StudentService studentService;

    @PostMapping("student")
    public Student createNewStudent(@RequestBody Student student){
        return studentService.createNew(student);
    }

    @GetMapping("students")
    public List<Student> findAllStudent(){
        return studentService.getAll();
    }

    @GetMapping("student")
    @Cacheable(key = "#idStudent", value = "Students")
    public Student findStudentById(@RequestParam String idStudent){
        return studentService.getById(idStudent);
    }

    @DeleteMapping("student")
    @CacheEvict(key = "#idStudent", value = "Students")
    public String deleteStudent(@RequestParam String idStudent){
        return studentService.delete(idStudent);
    }
}
