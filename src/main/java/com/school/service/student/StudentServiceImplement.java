package com.school.service.student;

import com.school.entity.student.Student;
import com.school.repository.StudentJPARepository;
//import com.school.repository.StudentRedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImplement implements StudentService{

//    @Autowired
//    StudentRedisRepository studentRepository;

    public static final String HASH_KEY = "Students";

    @Autowired
    StudentJPARepository studentJPARepository;

    @Autowired
    RedisTemplate redisTemplate;
    @Override
    public Student createNew(Student student) {
        studentJPARepository.save(student);
        redisTemplate.opsForHash().put(HASH_KEY, student.getIdStudent(), student);
        return student;
//        return studentRepository.createNewStudent(student);
    }

    @Override
    public String delete(String idStudent) {
        studentJPARepository.deleteById(idStudent);
        return "Student was removed!!!";
//        return studentRepository.deleteStudent(idStudent);
    }

    @Override
    public List<Student> getAll() {
        return studentJPARepository.findAll();
//        return studentRepository.findAllStudent();
    }

    @Override
    public Student getById(String idStudent) {
        System.out.println("called findStudentById() from DB");
//        return studentJPARepository.findById(idStudent).get();
        return (Student) redisTemplate.opsForHash().get(HASH_KEY, idStudent);
//        return studentRepository.findStudentById(idStudent);
    }


}
