package com.school.service.student;

import com.school.entity.student.Student;
import com.school.service.template.CreateResourceService;
import com.school.service.template.DeleteResourceService;
import com.school.service.template.GetAllResourceService;
import com.school.service.template.GetResourceByIDService;

public interface StudentService extends CreateResourceService<Student>, DeleteResourceService<String>, GetAllResourceService<Student>, GetResourceByIDService<Student, String> {

}
