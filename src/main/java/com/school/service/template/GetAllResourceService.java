package com.school.service.template;

import java.util.List;

public interface GetAllResourceService<T>{
    List<T> getAll();
}
