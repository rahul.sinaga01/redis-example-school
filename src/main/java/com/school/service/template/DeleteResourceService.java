package com.school.service.template;

public interface DeleteResourceService<ID> {
    String delete(ID id);
}
