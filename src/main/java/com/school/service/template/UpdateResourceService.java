package com.school.service.template;

public interface UpdateResourceService<T>{
    void update(T t);
}
