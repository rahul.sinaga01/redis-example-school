package com.school.service.template;

public interface CreateResourceService <T>{
    T createNew(T t);
}
