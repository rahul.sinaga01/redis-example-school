package com.school.service.template;

public interface GetResourceByIDService <T, ID>{
    T getById(ID id);
}
