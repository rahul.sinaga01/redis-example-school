//package com.school.repository;
//
//import com.school.entity.student.Student;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//
//@Repository
//public class StudentRedisRepository {
//
//    public static final String HASH_KEY = "Students";
//
//    @Autowired
//    private RedisTemplate template;
//
//    public Student createNewStudent(Student student){
//        template.opsForHash().put(HASH_KEY, student.getIdStudent(), student);
//        return student;
//    }
//
//    public List<Student> findAllStudent(){
//        return template.opsForHash().values(HASH_KEY);
//    }
//
//    public Student findStudentById(String idStudent){
//        System.out.println("called findStudentById() from DB");
//        return (Student) template.opsForHash().get(HASH_KEY, idStudent);
//    }
//
//    public String deleteStudent(String idStudent){
//        template.opsForHash().delete(HASH_KEY, idStudent);
//        return "Student was Removed";
//    }
//
//}
