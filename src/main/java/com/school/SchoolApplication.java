package com.school;

import com.school.repository.StudentJPARepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SchoolApplication {
//	private final StudentJPARepository studentJPARepository;
//
//	public SchoolApplication(StudentJPARepository studentJPARepository) {
//		this.studentJPARepository = studentJPARepository;
//	}

	public static void main(String[] args) {
		SpringApplication.run(SchoolApplication.class, args);
	}

}
